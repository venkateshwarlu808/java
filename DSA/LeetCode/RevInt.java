/*
 * 1. Reverse Integer (Leetcode:- 7)
 *
 * Given a signed 32-bit integer x, return x with its digits reversed. If reversing
 * x causes the value to go outside the signed 32-bit integer range [-231, 231
 * - 1], then return 0.
 *   Assume the environment does not allow you to store 64-bit integers (signed
 *   or unsigned).
 *   Example 1:
 *   Input: x = 123
 *   Output: 321
 *   Example 2:
 *   Input: x = -123
 *   Output: -321
 *   Example 3:
 *   Input: x = 120
 *   Output: 21
 *
 *   Constraints:
 *   -231 <= x <= 231 - 1
 */

import java.util.*;

class revInteger {
	
	static int reverseInteger(int num){
		int rev=0;
		while(num != 0){
			int cont=num % 10;
			if(rev>Integer.MAX_VALUE/10 || rev<Integer.MIN_VALUE/10){
				return 0;
			}
			rev=(rev*10)+ cont;
			num=num/10;
		}
		return rev;
	
	}

	public static void main(String[] mohit){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Number For Reverse");
		int num=sc.nextInt();
		System.out.println(reverseInteger(num));
	}
}
