/*
 * Q6. Product array puzzle
 * Problem Description
 * - Given an array of integers A, find and return the product array of the same
 *   size where the ith element of the product array will be equal to the
 *
 *   product of all the elements divided by the ith element of the array.
 *
 *   - Note: It is always possible to form the product array with integer (32 bit)
 *   values. Solve it without using the division operator.
 *
 *   Constraints
 *   2 <= length of the array <= 1000
 *   1 <= A[i] <= 10
 *   For Example
 *   Input 1:
 *   A = [1, 2, 3, 4, 5]
 *   Output 1:
 *   [120, 60, 40, 30, 24]
 *   Input 2:
 *   A = [5, 1, 10, 1]
 *   Output 2:
 *   [10, 50, 5, 50]
 */

import java.util.*;

class productArr {

	public static void main(String[] mohit){
		Scanner sc=new Scanner(System.in);
		
		int arr[]=new int[]{1,2,3,4,5};
		int prodArr[]=new int[arr.length];
		int prod=1;
		prodArr[0]=1;
		for(int i=0;i<arr.length;i++){
			prodArr[i]=prodArr[i-1]*arr[i-1];
				
		}
		for(int i=arr.length -1;i>=0;i--){
			prod=prod*arr[i];
			prodArr[i]=prodArr[i]*prod;
		//	prod=prod*arr[i];
		}
		System.out.println(prod);
		for(int i=0;i<arr.length;i++){
			System.out.print(prodArr[i]);
		}
		System.out.println("");
	}
}
		



