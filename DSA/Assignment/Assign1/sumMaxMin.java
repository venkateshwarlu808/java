
/*Q1. Max Min of an Array
 *
 * Problem Description
 * - Given an array A of size N.
 *   - You need to find the sum of the Maximum and Minimum
 *
 *   elements in the given array.
 *
 *   Problem Constraints
 *   1 <= N <= 105
 *   -109 <= A[i] <= 109
 *
 *   Example Input
 *   Input 1:
 *   A = [-2, 1, -4, 5, 3]
 *   Input 2:
 *   A = [1, 3, 4, 1]
 *
 *   Example Output
 *   Output 1:
 *   1
 *   Output 2:
 *   5
 *   Example Explanation
 *   Explanation 1:
 *   Maximum Element is 5 and Minimum element is -4. (5 + (-4)) = 1.
 *   Explanation 2:
 *   Maximum Element is 4 and Minimum element is 1. (4 + 1) = 5.
 */
import java.util.*;
class sumMaxMin {
	public static void main(String[] om){
		Scanner sc=new Scanner(System.in);
		int arr1[]=new int[]{-2,1,-4,5,3};
		int arr2[]=new int[]{1,3,4,1};
		int maxNum1=Integer.MIN_VALUE;
		int minNum1=Integer.MAX_VALUE;

		for(int i=0;i<arr1.length;i++){
			if(maxNum1 < arr1[i])
				maxNum1=arr1[i];
			if(minNum1 > arr1[i])
				minNum1=arr1[i];
		}
		System.out.println(maxNum1 + minNum1);
		int maxNum2=Integer.MIN_VALUE;
		int minNum2=Integer.MAX_VALUE;
		for (int i=0;i<arr2.length;i++){
			if(maxNum2 < arr2[i])
				maxNum2=arr2[i];
			if(minNum2 > arr2[i])
				minNum2=arr2[i];

		}
		System.out.println(maxNum2 + minNum2);
	}
}

