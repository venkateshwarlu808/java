
// WAP to print number in a Range. Take Range from user.

import java.util.*;

class PrimeDemo {
	public static void main(String[] mohit){
		Scanner sc=new Scanner(System.in);
		System.out.print("Enter Starting Number :");
		int start=sc.nextInt();
		System.out.print("Enter Ending Number :");
		int end=sc.nextInt();
		int flag;

		for(int i=start;i<=end;i++){
			
			if(i==1 || i==0)
				continue;
			//count++;
			flag=1;
			for(int j=2;j<i/2;j++){
				if(i%j==0){
					flag=0;
					break;
				}
			}
			if(flag==1)
				System.out.println(i);
		}
	}
}
			
