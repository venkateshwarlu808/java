
// WAP to check whether given string is palindrom or not.
// i/p1 : nayan   o/p1 : is Palindrom
// i/p2 : armstrong   o/p2 : not Palindrom


import java.util.*;
class palindrom {
	public static boolean isPalindrom(String str){
		int left=0, right=str.length()-1;
		while(left < right){
			if(str.charAt(left)!=str.charAt(right)){
				return false;
			}
			left++;
			right--;
		}
		return true;
	}

	public static void main(String[] mohit){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter String:");
		String str=sc.nextLine();

		if(isPalindrom(str)){
			System.out.println(str+" :is Palindrom");
		}else{
			System.out.println(str+" :is Not Palindrom");
		}
	}
}
