// WAP to find the occurance of vowels in given string
// i/p : adgtioseobi
// o/p : a=1,e=1,i=2,o=2\


class OccureVowel {
	public static void main(String[] mohit){
		String str="adgtioseobi";
		char ch[]=str.toCharArray();
		int a=0;
		int e=0;
		int i=0;
		int o=0;
		int u=0;

		for(int j=0;j<ch.length;j++){
			if(ch[j]=='a')
				a++;
			if(ch[j]=='e')
				e++;
			if(ch[j]=='i')
				i++;
			if(ch[j]=='o')
				o++;
			if(ch[j]=='u')
				u++;
		}
		System.out.println("a is "+ a +" times occures");
		System.out.println("e is "+ e +" times occures");
		System.out.println("i is "+ i +" times occures");
		System.out.println("o is "+ o +" times occures");
		System.out.println("u is "+ u +" times occures");
	}
}

