
//WAP to find the factorial of even number in given number
//i/p: 256946
//o/p: 2,720,24,720


class evenFacto {
	public static void main(String[] mohit){
		int num=256946;
		int arr[]=new int[10];
		int rev=0;
		int count=0;
		while(num != 0){
			int rem=num % 10;
		
			if(rem % 2==0){

				int sum=1;
				for(int i=1;i<=rem;i++){
					sum=sum*i;
				}
				arr[count]=sum;
				count++;
			}
			num=num/10;
			
		}	
		StringBuilder sb=new StringBuilder();
		for(int i=0;i<count;i++){
			sb.append(arr[i]);
			sb.append(", ");
		}
		sb.deleteCharAt(sb.length() - 1);
	 	sb.deleteCharAt(sb.length() - 1);

		System.out.println(sb);
	}
}

