
import java.util.*;

class StackDemo {
	public static void main(String[] args){
		Stack<Integer> s =new Stack<Integer>();

		s.push(10);
		s.push(20);
		s.push(30);
		s.push(40);

		System.out.println(s); // [10,20,30,40]
		System.out.println(s.size());  // 4 
		System.out.println(s.empty()); // false.  bcz stack is not empty.


		System.out.println(s.pop()); // 40 . in stack last data is removed firstly so 40 is removed
		System.out.println(s); // [10,20,30]
		System.out.println(s.peek());  //  30 .return the top elements of stack not delete the peek value only show
		System.out.println(s);  // [10,20,30]
		s.pop();
		s.pop();
		s.pop();
		System.out.println(s);  // [] .  stack empty  bcz all data in btn stack is poped
		/*s.pop();
		System.out.println(s);   Empty stack exception occurs bcz stack is empty and we use pop operation
		*/
		System.out.println(s.empty()); // true

	}
}
