
import java.util.*;

class Stack {
	int maxSize;
	int stackArr[];
	int top=-1;

	Stack(int size){
		this.stackArr=new int[size];
		this.maxSize=size;
	}
	
	void push(int data){
		if(top == maxSize-1){
			System.out.println("Stack Overflow");
			return;
		}else{
			top++;
			stackArr[top]=data;
		}
	}

}

class Client {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter Stack Size");
		int size=sc.nextInt();
		Stack s=new Stack();
		char ch;

		do {
			System.out.println("1.Push\n 2.Pop\n 3.Peek\n 4.Empty\n 5.Size\n 6.PrintStack ");

			System.out.print("Enter your choice ");
			int choice=sc.nextInt();

			switch(choice){
				case 1:
					{
						System.out.println("Enter Elements");
						int data=sc.nextInt();
						s.push(data);
					}
					break;

				default :
					System.out.println("Wrong Input");
					break;
			

			}
			System.out.println("Do You Want To Continue ");
			ch=sc.next().charAt(0);
		}while(ch =='Y' || ch =='y');
	}
}
