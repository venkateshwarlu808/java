import java.util.*;

class Node {
	int data;
	Node next=null;
	Node(int data){
		this.data=data;
	}
}
class LinkedList {
	Node head=null;

	void addFirst(int data){
		Node nn=new Node(data);	

		if(head==null){
			head=nn;
		}else{
			nn.next=head;
			head=nn;
		}
	}
	void printSLL(){
		if(head==null){
			System.out.println("Empty LL");
		}else{
			Node temp=head;
			while(temp.next != null){
				System.out.print(temp.data+" -> ");
				temp=temp.next;
			}
			System.out.println(temp.data);
		}
	}

}


class Client {
	public static void main(String[] args){
		LinkedList ll=new LinkedList();

		char ch;

		do{
			System.out.println("Singly LinkedList");
			System.out.println("1.addFirst ");
			System.out.println("2.addLast ");
			System.out.println("3.addAtPos ");
			System.out.println("4.delFirst ");
			System.out.println("5.addLast ");
			System.out.println("6.delAtPos ");
			System.out.println("7.countNode ");
			System.out.println("8.printSLL ");

			
			System.out.println("Enter your Choice ");
			Scanner sc=new Scanner(System.in);
			int choice=sc.nextInt();
			switch(choice){
				case 1:{
					       System.out.println("Enter data");
					       int data=sc.nextInt();
					       ll.addFirst(data);
					}
				       break;
				case 2:{
					       System.out.println("Enter data");
					}
				       break;
				case 8:
				       		ll.printSLL();
						break;

				default :
						System.out.println("Wrong Input");
						break;

				
			}
			System.out.println("Do You Want To Continue ? ");
			ch=sc.next().charAt(0);
		}while(ch=='Y' || ch=='y');
	}
}



