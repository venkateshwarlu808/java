
class revPrintNum {
	static void numPrintRev(int num){
		if(num==0){
			return;
		}
		
		System.out.println(num);
		numPrintRev(num-1);
	}
	public static void main(String[] args){
		numPrintRev(10);
	}
}

/*
 10
 9
 8
 7
 6
 5
 4
 3
 2
 1
*/
