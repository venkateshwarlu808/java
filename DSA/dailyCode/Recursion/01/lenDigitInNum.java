
class LenDigitNum {
	static int lenDigitOfNum(int num){
		if(num/10==0){
			return 1;
		}
		return lenDigitOfNum(num/10)+1;
	}
	public static void main(String[] args){
		System.out.println(lenDigitOfNum(1234));
	}
}
