

class SumNatural{
	static int sumNaturalN(int num){
		if(num==0){
			return num;
		}
		return sumNaturalN(num-1)+num;
	}
	public static void main(String[] args){
		System.out.println(sumNaturalN(10));
	}
}
