
// Given an array of size N & Q number of queries . 
// Query contains two parameters (s,e)
// for all queries print sum of all elements from index s to e 
// Arr=[-3,6,2,4,5,2,8,-9,3,1]
// n=10 , Q=3
//
// s    e    sum
// 1    3     12
// 2    7     12
// 1    1     6



import java.io.*;

class quryRangeSum {
	public static void main(String[] mohit)throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int arr[]=new int[]{-3,6,2,4,5,2,8,-9,3,1};
		System.out.println("Enter Numb Of Queries");
		int Q=Integer.parseInt(br.readLine());
		 for(int j=0;j<Q;j++){
			 System.out.println("Enter starting index");
			 int s=Integer.parseInt(br.readLine());
			 System.out.println("Enter ending Index");
			 int e=Integer.parseInt(br.readLine());
			 int sum=0;
			 for(int i=s;i<=e;i++){
				sum=sum + arr[i];
			 }
			 System.out.println(sum);
		 }
	}
}

