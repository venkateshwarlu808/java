
// Reverse Array


class revArr {
	public static void main(String[] args){
		int arr[]=new int[]{1,2,3,4,5,6,7};
		int N=7;

		int i=0;
		int j=N-1;
		while(i<j){
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
			i++;
			j--;
		}
		for(int k=0;k<N;k++){
			System.out.println(arr[k]);
		}
	}
}
