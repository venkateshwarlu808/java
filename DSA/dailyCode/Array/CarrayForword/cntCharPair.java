
// Given the count of pair (i,j) such that 
// a) i < j
// b)arr[i]='a'
//   arr[j]='g'

//   Arr[a,b,e,g,a,g]
//   o/p: 3

class cntPair {
	public static void main(String[] mohit){
		int arr[]=new int[]{'a','b','e','g','a','g'};
		int count=0;

		for(int i=0;i<arr.length;i++){
			for(int j=i+1;j<arr.length;j++){
				if(arr[i]=='a' && arr[j]=='g')
					count++;
			}
		}
	
		System.out.println(count);
	}
}
