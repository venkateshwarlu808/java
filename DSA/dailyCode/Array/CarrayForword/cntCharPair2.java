
// Given the count of pair (i,j) such that 
// a) i < j
// b)arr[i]='a'
//   arr[j]='g'

//   Arr[a,b,e,g,a,g]
//   o/p: 3
//                              Less iterative


class cntPair {
	public static void main(String[] mohit){
		int arr[]=new int[]{'b','a','a','a','c','g','g','g'};
		int count=0;
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'){
				for(int j=i+1;j<arr.length;j++){
					if(arr[j]=='g')
						count++;
				}
			}
		}
	
		System.out.println(count);
	}
}
