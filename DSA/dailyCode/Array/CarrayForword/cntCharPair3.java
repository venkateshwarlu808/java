
// Given the count of pair (i,j) such that 
// a) i < j
// b)arr[i]='a'
//   arr[j]='g'

//   Arr[a,b,e,g,a,g]
//   o/p: 3
//                              Less iterative


class cntPair {
	public static void main(String[] mohit){
		int arr[]=new int[]{'b','a','a','a','c','g','g','g'};
		int count=0;
		int pair=0;
		
		for(int i=0;i<arr.length;i++){
			if(arr[i]=='a'){
				count++;
			}else if(arr[i]=='g'){
				pair=pair+count;
			}
		}
		System.out.println(pair);	
	}
}
