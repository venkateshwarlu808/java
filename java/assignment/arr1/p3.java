
// WAP to take size from user and also take integer elements from user Print product of odd index only.


import java.io.*;
class OddIndex{

	public static void main(String[] om)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the Size Of Array:");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];
		int product=1;

		System.out.println("Enter Array Elements:");
		
		for(int i=0;i<arr.length;i++){
			
			arr[i]=Integer.parseInt(br.readLine());
			
			if(i%2==1){

				product=product*arr[i];
			}
		}
		System.out.println("Product of Odd Index is=" +product);
	}
}
