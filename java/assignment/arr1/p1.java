
// WAP to take size of array from user and also take integer elements from user and print sum of odd elements only

import java.io.*;
class oddSum{
	public static void main(String[] himanshu)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Size Of Array");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int oSum=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2 !=0){
				oSum=oSum+arr[i];
			}
		}
		System.out.println("Sum Of Odd Elements Is="+oSum);
	}
}


