
// WAP to take size of Array from user and also take integer elements from user, print product of even elements only

import java.io.*;
class evenProduct{
	public static void main(String[] harshita)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int product=1;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				product=product*arr[i];
			}
		}
		System.out.println("Product Of Even Elements ="+ product);
	}
}

