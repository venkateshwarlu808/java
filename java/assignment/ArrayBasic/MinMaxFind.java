
/*
 Given an array A of size N of integers. Your task is to find the minimum and
 maximum elements in the array.
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int min,max;
		min=max=arr[0];
		for(int i=1;i<size;i++){
			if(max < arr[i]){
				max=arr[i];
			}else{
				min=arr[i];
			}
		}
		System.out.println("Maximum Number is =" + max);
		System.out.println("Minimum Number is =" + min);

	}
}



