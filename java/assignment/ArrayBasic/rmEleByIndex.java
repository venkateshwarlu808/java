
/*
 9]   Given an array of a fixed length. The task is to remove an element at a specific
 index from the array.
 .
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
	
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		System.out.println("Enter Removing index Number");
		int indexNo=Integer.parseInt(br.readLine());
		for(int i=indexNo;i < size - 1;i++){
			arr[i]=arr[i+1];
	
		}
		size=size-2;
		for(int i=0;i<=size;i++){
			System.out.print(arr[i]+" ");
		}
	}
}
		
		

	




