
/*
11]     Given two arrays of A and B respectively of sizes N1 and N2, the task is to
 calculate the product of the maximum element of the first array and minimum
 element of the second array..
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr1[]=new int[size];
		int arr2[]=new int[size];
		System.out.println("Enter 1st Array elementys");
		for(int i=0;i<size;i++){
			
			arr1[i]=Integer.parseInt(br.readLine());
		
			
		}
		System.out.println("Enter 2nd Array Elements");
		for(int i=0;i<size;i++){
			arr2[i]=Integer.parseInt(br.readLine());
		}
		int min,max;
		max=arr1[0];
		min=arr2[0];
		for(int i=1;i<size;i++){
			if(max < arr1[i]){
				max=arr1[i];
			}
		}
		for(int i=1;i<size;i++){
			if(min > arr2[i]){
				min=arr2[i];
			}
		}
		System.out.println("Product of Maximum and Minimum Number is =" + max*min);
		//System.out.println("Minimum Number is =" + min);

	}
}



