
/*
 You are given an integer N. You need to convert all zeros of N to 5
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int i=0;i<size;i++){
			if(arr[i]==0){
				arr[i]=5;
			}
			
		System.out.println(arr[i]);
		}
		
		

	}
}



