
/*
 This is a functional problem. Your task is to return the product of array elements
 under a given modulo.
 The modulo operation finds the remainder after the division of one number by
 another. For example, K(mod(m))=K%m= remainder obtained when K is divided
 by m
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int Product=1;
		for(int i=1;i<size;i++){
			Product=Product*arr[i];
		}
		System.out.println("Product of Array Elements is =" + Product);
		

	}
}



