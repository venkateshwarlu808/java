/*
Given an integer array and another integer element. The task is to find if the given
element is present in the array or not.
*/
import java.io.*;
class ArrayDemo {

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}

		System.out.println("Enter Search Element");
		int search=Integer.parseInt(br.readLine());
		
		int flag=0;
		for(int i=0;i<size;i++){
			if(arr[i]==search){
				flag=1;
			}else{
				flag=0;
			}

		}

		if(flag==1){
			System.out.println("Element is found");
		}else{
			System.out.println("Element is not Found");
		}

	}
}


