
/*
 Given an array A[] of size n. The task is to find the largest element in it..
 */

import java.io.*;

class ArrayDemo{

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int lrgstNum=arr[0];
		for(int i=1;i<size;i++){
			if(lrgstNum < arr[i]){
				lrgstNum=arr[i];
			}
		}
		System.out.println("Largest Number is =" + lrgstNum);
		

	}
}



