
/*
 6]   Given an array arr[] containing positive elements. A and B are two numbers
 defining a range. The task is to check if the array contains all elements in the given
 range.
 */

import java.io.*;

class ArrayDemo{

	public static boolean eleInRange(int arr[],int size,int strNo,int endNo) {
		
		if(strNo > endNo){
			return false;
		}
		for(int i=strNo;i<=endNo;i++){
			boolean found=false;
			for(int j=0;j<size;j++){
				if(arr[j]==i){
					found=true;
					break;
				}
			}
			if(!found){
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];
		
		System.out.println("Enter Starting Numb");
		int strNo=Integer.parseInt(br.readLine());
				
		System.out.println("Enter Ending Numb");
		int endNo=Integer.parseInt(br.readLine());
		System.out.println("Enter Array Elements");
		for(int i=0;i<size;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		if(eleInRange(arr,size,strNo,endNo)){
			System.out.println("Yes");
		}else{
			System.out.println("No");
		}
	}
}
		
		

	




