

/* 
 * 12]     First and last occurrences of X :
 * Given a sorted array having N elements, find the indices of the first and last
 * occurrences of an element X in the given array.
 * Note: If the number X is not found in the array, return '-1' as an array.
 */

class ArrayDemo {
	public static void main(String[] Bhagu){
		int arr[]={1,3,3,4};
		int search=3;

		int firstIndex=-1;
		int lastIndex=-1;

		for(int i=0;i<arr.length;i++){
			if(arr[i]==search && firstIndex==-1){
				firstIndex=i;
			}
			if(arr[i]==search && firstIndex !=-1){
				lastIndex=i;
			}
		}
		System.out.println("First Occurance is :"+ firstIndex);
		System.out.println("Last Occurance is :"+ lastIndex);
	}
}



