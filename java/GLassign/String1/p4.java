

// WAP  to find Min and Max element in array

import java.io.*;
class MaxMinCheck{

	public static void main(String[] mohit)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("Enter Array Elements");
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int max=0;
		int min=0;

		for(int i=0;i<arr.length;i++){
			if(arr[i]>max){
				max=arr[i];
			}
		}
		System.out.println("Max Element Is="+max);

		for(int i=0;i<arr.length;i++){
			if(arr[i]<max){
				max=arr[i];	
				min=arr[i];
			}
		}
		System.out.println("Min Element is="+min);
	}
}


