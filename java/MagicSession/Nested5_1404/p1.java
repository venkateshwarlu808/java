
// D4  C3  B2  A1
// A1  B2  C3  D4
// D4  C3  B2  A1
// A1  B2  C3  D4



import java.io.*;
class Nested5{
	public static void main(String[] args)throws IOExeption{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		int row=Integer.parseInt(br.readLine());

		int num=row;
		int ch=64+row-1;
		int n=row-row+1;
		int ch1=64;

		for(int i=1;i<=row;i++){
			for(int j=1;j<=row;j++){
				if(i%2==1){
					System.out.print((char)(ch)+num+" ");
					ch--;
					num--;
				}else{
					System.out.print((char)ch1+n+" ");
					ch1++;
					n++;
				}
			}
			System.out.println(" ");
		}
	}
}
