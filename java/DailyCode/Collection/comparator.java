

// it is used to sort on ArrayList


import java.util,*;

class Employee {

	String empName=null;
	float sal=0.0f;

	Employee(String empName,float sal){
		this.empName=empName;
		this.sal=sal;
	}
	public int toString(){
		return "{" + empName + ":" + sal + "}";
	}
}

class SortByName implements comparator <Employee> {
	public int compare(Employee obj1,Employee obj2){
		return obj1.empName.compareTo(obj2.empName);
	}
}
class SortBySal implements comparator <Employee> {
	public int compare(Employee obj1,Employee obj2){
		return (int)(obj1.sal.obj2.sal);
	}
}

class ListSortDemo {
	public static void main(String[] mohit){
		ArrayList <Employee> al=new ArrayList <Employee>();
		al.add(new Employee("Kanha",2000000.00f));
		al.add(new Employee("Ashish",2500000.00f));
		al.add(new Employee("Badhe",1500000.00f));
		al.add(new Employee("Rahul",1750000.00f));

		System.out.println(al);
		
		Collection.sort(al,new SortByName());
		System.out.println(al);

		Collection.sort(al,new SortBySal());
		System.out.println(al);
	}
}




