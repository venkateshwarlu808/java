

import java.util.*;

class Movie implements Comparable {

	String movieName=null;
	float totColln=0.0f;
	Movie(String movieName , float totColln){
		this.movieName=movieName;
		this.totColln=totColln;
	}
	public int compareTo(Object obj){
		return -(movieName.compareTo(((Movie)obj)movieName));
	}
	public String toString(){
		return movieName;
	}
}

class TreeSetDemo {
	public static void main(String[] mohit){
		TreeSet ts=new TreeSet();
		ts.add(new Movie("Gadar2",150.00f));
		ts.add(new Movie("OMG2",120.00f));
		ts.add(new Movie("Jailer",200.00f));
		ts.add(new Movie("OMG2",120.00f));		

		System.out.println(ts);
	}
}


