
import java.io.*;
class EvenOddCount{
	public static void main(String[]mohit)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Array Size");
		int size=Integer.parseInt(br.readLine());
		System.out.println("Enter Array Elements");
		int arr[]=new int[size];
		for(int i=0;i<arr.length;i++){
			arr[i]=Integer.parseInt(br.readLine());
		}
		int evenCount=0;
		int oddCount=0;
		for(int i=0;i<arr.length;i++){
			if(arr[i]%2==0){
				evenCount++;
			}else{
				oddCount++;
			}
		}
		System.out.println("Even Count is="+ evenCount);
		System.out.println("Odd Count is="+ oddCount);
	}
}
