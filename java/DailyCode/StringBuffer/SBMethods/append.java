

//                                  public synchronized StringBuffer append(String str);

// Description : Append the <code>String</code> to this </code> .
//               if str is null,the String "null" is appended.
// Parameters : String(str the <code>String</code> to append).



class AppendDemo {

	public static void main(String[] mohit){

		StringBuffer str1=new StringBuffer("Hello");
		String str2="World";
		str1.append(str2);
		System.out.println(str1);
	}
}
