

class StringBufferDemo{

	public static void main(String[] om){

		StringBuffer str1=new StringBuffer("Mohit");
		
		System.out.println(System.identityHashCode(str1)); // 225534817
		System.out.println(str1.capacity()); //21

		str1.append("Badgujar");

		System.out.println(str1); // MohitBadgujar
		System.out.println(System.identityHashCode(str1)); // 225534817
	}
}

