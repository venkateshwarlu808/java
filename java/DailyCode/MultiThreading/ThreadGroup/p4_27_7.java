




class MyThread extends Thread {

	MyThread(ThreadGroup tg,String str){
		super(tg,str);
	}

	public void run(){
		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}

		
	}
}

class ThreadGroupDemo {
	public static void main(String[] mohit){
		ThreadGroup pThreadGP=new ThreadGroup("India");

		MyThread t1=new MyThread(pThreadGP,"Maharashtra");
		MyThread t2=new MyThread(pThreadGP,"Goa");
		t1.start();
		t2.start();

		ThreadGroup cThreadGP1=new ThreadGroup(pThreadGP,"Pak");
		MyThread t3=new MyThread(cThreadGP1,"Karachi");
		MyThread t4=new MyThread(cThreadGP1,"Lahore");
		t3.start();
		t4.start();

		ThreadGroup cThreadGP2=new ThreadGroup(pThreadGP,"Ban");
		MyThread t5=new MyThread(cThreadGP2,"Dhanka");
		MyThread t6=new MyThread(cThreadGP2,"Mirpur");
		t5.start();
		t6.start();

		cThreadGP1.interrupt();
		System.out.println(pThreadGP.activeGroupCount());
		System.out.println(pThreadGP.activeGroupCount());

	}
}


