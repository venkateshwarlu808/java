

//                         public boolean equals(Object anObject);
//
// Description :- predicate which compares anObject to this . This is true only for String with the same character sequence ,return true if the anObject is 
//                semantically equal to this.
// Parameter :- Object(anObject).
// return type :- boolean.                        



class EqualsDemo{

	public static void main(String[] akshu){

		String str1="String";
		String str2=new String("String");
		String str3="string";

		System.out.println(str1.equals(str2));// true
		System.out.println(str1.equals(str3));// false
	}
}

