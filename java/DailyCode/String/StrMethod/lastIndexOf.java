

//                                      public int lastIndexOf(int ch,int upto Index);
//  
//  Description :- find last instance of the character in the given string .
//  Parameter :- character (ch to find), Integer (index to start the search).
//  return type :- Integer.
                                    


class LastIndexOf{

	public static void main(String [] om){

		String str1="Shashi";

		System.out.println(str1.lastIndexOf('h',0));
		System.out.println(str1.lastIndexOf('h',1));
		System.out.println(str1.lastIndexOf('h',3));
		System.out.println(str1.lastIndexOf('s',5));
	}
}

