
//                                  public int compareToIgnoreCase(String str);
// Description :- It compare str1 & str2 (case insensetive).
// Parameter :- String .
// return Type :- Integer.


class CompareToIgnoreCaseDemo{
	
	public static void main(String[] chetu){

		String str1="MOHIT";
		String str2="mohit";
		String str3="MohitRao";

		System.out.println(str1.compareToIgnoreCase(str2));
		System.out.println(str1.compareToIgnoreCase(str3));
	}
}
