

//                               public boolean equalsIgnoreCase(String anotherString);
//
// Description :- Compares the string to this string ignore case .
// Parameter :- String(str2).
// return type :- boolean.



class EqualsIgnoreCaseDemo{

	public static void main (String[] om){

		String str1="Mohit";
		String str2=new String("moHit");

		System.out.println(str1.equalsIgnoreCase(str2));
	}
}
