
//                                      public String substring(int index);
//
//  Description :- create a substring of the given string starting at a specified index & ending at the end of given string.
//  Parameters :- Integer (index of the String);
//  return type :- String.




class Substring{

	public static void main(String[] om){

		String str1="Core2Web Tech";

		System.out.println(str1.substring(5));
		System.out.println(str1.substring(0,3));
	}
}
