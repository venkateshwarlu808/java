

//                                            public String split(String delimiter);

// Description :  Splits this string around matches of regular expressions.
// Parameter : delimiter (pattern to match)
// return type : String[] (array of split Strings).



class SplitDemo {

	public static void main(String[] om){

		String str="Know The Code Till The Core";
		String[] strResult=str.split(" ");

		for(int i=0;i<strResult.length;i++){

			System.out.println(strResult[i]);
		}
	}
}
