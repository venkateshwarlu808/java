

//                      public char charAt(int index);
//
// Description :- It returns the character located at specified index within the given string.
// Parameter :- integer(index)
// return Type :- character.


class CharAtDemo{

	public static void main(String[] chetu){

		String str="Core2Web";

		System.out.println(str.charAt(4));  // 2
		System.out.println(str.charAt(0));  // c

		System.out.println(str.charAt(8));  // String index out of bound exception.
		System.out.println(str.charAt(7));
	}
}

