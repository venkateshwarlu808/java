

//                               public boolean startsWith(String prefix,int toffset);

// Description :predicate  which determines if the given String contains the given prefix begining comparison at toffset.
//              The result is negative or greater than str.length().
// Parameters: prefix String to compare,toffset for this string where the comparison Starts.
// return Type : boolean.




class StartsWithDemo{

	public static void main(String[] args){

		String str="Core2Web";
		System.out.println(str.startsWith("or",1));
	}
}
