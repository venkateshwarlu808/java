
// Description :- concatinate String to this string i.e. Another String is concatinated with the first String.
//                Impliments new Array of character whose length is sum of str1.length and str2.length.
// Parameter :- String
// return Type :- String.



class ConcatDemo{

	public static void main(String[] kadu){

		String str1="Core2";
		String str2="Web";
		String str3=str1.concat(str2);

		System.out.println(str3);
	}
}

