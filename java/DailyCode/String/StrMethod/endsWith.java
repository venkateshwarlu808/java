

//                                    public boolean endsWith(String suffix);

// Description : predicate which determines if the string ends with given suffix .
//               If the suffix is an empty String , true is returned.
//               Throws NullPointerException if suffix is null.
// Parameters : prefix String to compare
// return type : boolean.



class EndsWithDemo{

	public static void main(String[] mohit){

		String str="Know the code till the core";
		System.out.println(str.endsWith("the"));
		System.out.println(str.endsWith("core"));
	}
}
