
//                                     public int indexOf(char ch,int fromIndex);
//
// Description :- Find the instance of the character in the given STring.
// Parameter :- character (ch to find ) , Integer (index to start the search).
// return Type :- Integer. . 


class IndexOfDemo{

	public static void main(String[] om){

		String str1="Mohit";

		System.out.println(str1.indexOf('M',0));
		System.out.println(str1.indexOf('M',1));
		System.out.println(str1.indexOf('t',4));
	}
}
