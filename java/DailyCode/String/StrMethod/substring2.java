
//                          public String substring(int i,int j);

// description : creates a substring of the given String starting at a specified index and ending at one character before the specified index.
// Parameter : Integer(starting index),Integer(Ending Index).
// return type : String.



class MySubstring {

	public static void main(String[] akshay){

		String str="Core2Web";
		System.out.println(str.substring(5,8));
	}
}
//                          public String substring(int i,int j);

// description : creates a substring of the given String starting at a specified index and ending at one character before the specified index.
// Parameter : Integer(starting index),Integer(Ending Index).
// return type : String.



class MySubstringDemo {

	public static void main(String[] akshay){

		String str="Core2Web";
		System.out.println(str.substring(5,8));
		System.out.println(str.substring(0,4));
	}
}
