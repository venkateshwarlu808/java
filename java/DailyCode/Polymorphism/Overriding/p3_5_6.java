




class Parent {
	Parent (){
		System.out.println("Parent Constructor");
	}
	void fun(){
		System.out.println("In Parent Fun ");
	}
}

class Child extends Parent {
	Child (){
		System.out.println("In Child Constructor");
	}
	void Fun (){
		System.out.println("In Child Fun ");
	}
}

class Client {
	public static void main(String[] mohit){
		Parent obj1 = new Child ();
		obj1.fun();
	}
}
