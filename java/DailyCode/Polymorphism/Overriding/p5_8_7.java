


class Parent {
	public void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent {
	void fun(){
		System.out.println("Child fun");
	}
}
class Client {
	public static void main(String[] mohit){
		Child obj=new Child();
		obj.fun();
	}
}

// error : fun() in Child cannot override fun() in Parent
//         attempting to assign weaker access privileges; was public
