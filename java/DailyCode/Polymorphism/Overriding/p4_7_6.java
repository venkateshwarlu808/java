



class Parent {
	Object fun(){                                          //  char fun(){   
		System.out.println("Parent fun");
		return new Object();                          //  return 'A';         
	}
}

class Child extends Parent {                                  // int fun(){
	String fun(){
	       System.out.println("Child fun");
	       return "Shashi";                              // return 0;
	}
}
class Client {
	public static void main(String[] mohit){
		Parent p=new Child();
		p.fun();
	}
}
//
//Op: Child fun                                              // error: fun in child cannot override fun() in parent


							     //        return type int is not compatible with char
