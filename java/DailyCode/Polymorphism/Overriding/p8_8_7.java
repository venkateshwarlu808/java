
// Access Specifier in Overriding


class Parent {
	final void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent {
	void fun(){
		System.out.println("Child fun");
	}
}
class Client {
	public static void main(String[] mohit){
		Parent obj=new Parent();
		obj.fun();
	}
}


// error :
//        overriding method is final
