

//  Static Modifier in Overriding



class Parent {
	static void fun(){
		System.out.println("Parent fun");
	}
}
class Child extends Parent {
	void fun(){
		System.out.println("Child fun");
	}
}
class Client {
	public static void main(String[] mohit){
		Parent obj=new Child();
		obj.fun();
	}
}

// error : fun() in child cannot override fun() in parent
